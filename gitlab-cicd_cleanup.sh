#!/usr/bin/bash
# Based on https://stackoverflow.com/a/73820571
# and https://benlobaugh.medium.com/how-to-clean-up-gitlab-artifacts-c7ce34c70213

project_id=""
api_token=""

time_expire=$(( 7*24*60*60 ))

ids=()
page_start=2
page_end=2

page=$page_start
while (( page <= page_end )) && response="$(curl -s --header "PRIVATE-TOKEN: ${api_token}" \
        "https://git.lwp.rug.nl/api/v4/projects/${project_id}/pipelines?page=$page&per_page=100")"; do
    ids+=("$(jq '.[].id' <<< "$response" | tr '\n' ' ')")
    # for i in {1..100}; do
    #     pipeline="$(jq ".[${i}]" <<< "$response")"
    #     id="$(jq ".id" <<< "$pipeline")"
    #     # time format: 2024-03-08T15:31:22.496Z
    #     time="$(jq ".updated_at" <<< "$pipeline")"
    #     # TODO: compare time and filter
    # done
    (( page++ ))
done
# WARNING: dangerous part
# for id in "${ids[@]}"; do
#     curl -s --header "PRIVATE-TOKEN: ${api_token}" --request DELETE \
#         "https://git.lwp.rug.nl/api/v4/projects/${project_id}/pipelines/${id}" || \
#         break
# done
