## CI/CD

### GitLab Runners

#### Docker

##### Rootless

It is possible to use an unprivileged, rootless Docker container for a GitLab runner.
See `gitlab-runner-docker-rootless_start.sh` and `gitlab-runner-docker-rootless_config.toml`.
