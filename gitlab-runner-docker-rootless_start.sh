#!/bin/bash
set -eux
[ -z ${DOCKER_HOST+x} ] &&\
    [ -e /run/user/$(id -u)/docker.sock ] &&\
    export DOCKER_HOST=unix:///run/user/$(id -u)/docker.sock

[ -e ~/.config/gitlab-runner ] ||\
    mkdir ~/.config/gitlab-runner

if docker inspect --format="{{.State.Running}}" gitlab-runner >/dev/null 2>&1; then
    docker start gitlab-runner
else 
    docker run -d --name gitlab-runner --restart always\
      -v $HOME/.config/gitlab-runner:/etc/gitlab-runner \
      -v ${DOCKER_HOST#*://}:/var/run/docker.sock \
      --env TZ=Europe/Amsterdam \
      gitlab/gitlab-runner
fi
